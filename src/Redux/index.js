import UserProfile from './UserProfile/UserProfileReducers'
import Appconfig from './Appconfig/AppconfigReducers'
import Tracestats from './Tracestats/TracestatsReducers'

import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import ReduxThunk from 'redux-thunk'

const AppReducer = combineReducers({
  UserProfile,
  Appconfig,
  Tracestats
})

// const MyStore = createStore(appReducer, {}, compose(applyMiddleware(ReduxThunk), autoRehydrate()))
const appStore = createStore(AppReducer, {}, compose(applyMiddleware(ReduxThunk)))

export default appStore
