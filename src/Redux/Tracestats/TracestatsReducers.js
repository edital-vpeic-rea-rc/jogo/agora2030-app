import { TRACESTATS_ANDAMENTO, TRACESTATS_UPDATE } from './TracestatsTypes'

const INITIAL_STATE = {}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TRACESTATS_ANDAMENTO:
      return { ...state, updatingData: !state.updatingData }
    case TRACESTATS_UPDATE:
      // console.log(action.payload)
      return { ...state, ...action.payload }

    default:
      return state
  }
}
