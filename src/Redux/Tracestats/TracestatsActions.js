import { TRACESTATS_ANDAMENTO, TRACESTATS_UPDATE } from './TracestatsTypes'
import { getTracestats } from '../../Api/tracestats'
import { AsyncStorage } from 'react-native'

const getTracestatsAction = (token = '') => {
  return dispatch => {
    dispatch({
      type: TRACESTATS_ANDAMENTO
    })
    getTracestats(token)
      .then(
        success => {
          dispatch({
            type: TRACESTATS_ANDAMENTO
          })
          dispatch({
            type: TRACESTATS_UPDATE,
            payload: success.data
          })
        },
        error => {
          console.log(error)
          if (error) {
            AsyncStorage.removeItem('apiToken', () => {
              console.log('removido')
            })
          }
          dispatch({
            type: TRACESTATS_ANDAMENTO
          })
        }
      )
  }
}

export {
  getTracestatsAction
}
