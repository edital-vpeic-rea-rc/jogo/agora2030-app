import {
  SHOW_TUTORIAL
} from './AppconfigTypes.js'

const modificaTutorial = (value) => {
  return dispatch => {
    dispatch({
      type: SHOW_TUTORIAL
    })
  }
}

export {
  modificaTutorial
}
