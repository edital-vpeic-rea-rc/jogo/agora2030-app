import { Toast } from 'native-base'
import GoogleSignIn from 'react-native-google-sign-in'
import {
  socialLogin,
  socialSignUp,
  signUp,
  fbGetUserData
} from '../../Api/auth'
import { editProfile } from '../../Api/profile'
import {
  LoginManager,
  AccessToken
} from 'react-native-fbsdk'

import {
  MODIFICA_RECEIVEPUSH,
  MODIFICA_RECEIVEEMAIL,
  MODIFICA_API_TOKEN,
  GOOGLE_LOGIN,
  FACEBOOK_LOGIN,
  UPDATE_USER_PROFILE
} from './UserProfileTypes'

const _socialSignUp = (data, dispatch) => {
  console.log(data)
  return new Promise((resolve, reject) => {
    socialSignUp(data)
    .then(
      success => {
        Toast.show({
          text: 'Conta criada com sucesso!',
          position: 'bottom',
          duration: 3000
        })
        dispatch({
          type: MODIFICA_API_TOKEN,
          payload: success.data
        })
        resolve(success)
      },
      error => {
        console.log('Erro ao fazer cadastro via social: ', error.response)
        Toast.show({
          text: error.response.data.message,
          position: 'bottom',
          duration: 3000
        })
        reject(error)
      }
    )
  })
}

const modificaReceiveEmail = () => {
  return {
    type: MODIFICA_RECEIVEEMAIL
  }
}

const modificaReceivePush = () => {
  return {
    type: MODIFICA_RECEIVEPUSH
  }
}

const updateUserProfile = (v) => {
  return {
    type: UPDATE_USER_PROFILE,
    payload: v
  }
}

const doRegister = (data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      if (data.socialLoginOrigin && data.socialLoginOrigin.length > 0) {
        _socialSignUp(data)
        .then(
          success => {
            resolve(success)
          },
          error => {
            reject(error)
          }
        )
      } else {
        signUp(data)
        .then(
          success => {
            alert('Conta criada com sucesso! Confirme o seu email antes de fazer login.')
            resolve(success)
          },
          error => {
            console.log(error.response)
            Toast.show({
              text: error.response.data.message,
              position: 'bottom',
              duration: 3000
            })
            reject(error)
          }
        )
      }
    })
  }
}

const doEditProfile = (data) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      editProfile('', data)
      .then(
        success => {
          dispatch({
            type: UPDATE_USER_PROFILE,
            payload: { ...data, ...success.data }
          })
          console.log('perfil atualizado', { ...data, ...success.data })
          // dispatch(NavigationActions.navigate({ routeName: 'Config' }))
          Toast.show({
            text: 'Perfil atualizado com sucesso!',
            position: 'bottom',
            duration: 3000
          })
          resolve(success)
        },
        error => {
          console.log(error.response)
          Toast.show({
            text: error.response.data.message,
            position: 'bottom',
            duration: 3000
          })
          reject(error)
        }
      )
    })
  }
}

const googleRegisterOrLogin = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      GoogleSignIn.signInPromise()
        .then(
          user => {
            console.log(user)
            dispatch({
              type: GOOGLE_LOGIN,
              payload: user
            })
            let dataLogin = {
              socialLoginOrigin: 'google',
              email: user.email,
              accessToken: user.accessToken
            }
            if (user.photo) {
              dataLogin.socialAvatar = user.photo
            }
            socialLogin(dataLogin)
              .then(
                success => {
                  if (success.data.newUser) {
                    const u = {
                      email: user.email || '',
                      socialAvatar: user.photo || '',
                      firstName: user.givenName || '',
                      lastName: user.familyName || user.givenName || '',
                      socialLoginOrigin: 'google'
                    }
                    // dispatch(NavigationActions.navigate({ routeName: 'Register' }))
                    _socialSignUp(u, dispatch)
                      .then(
                        success => {
                          resolve(success)
                        },
                        error => {
                          reject(error)
                        }
                      )
                  } else {
                    dispatch({
                      type: MODIFICA_API_TOKEN,
                      payload: success.data.data
                    })
                    dispatch({
                      type: UPDATE_USER_PROFILE,
                      payload: success.data.data
                    })
                  }
                  resolve(success)
                },
                error => {
                  console.log('Erro ao pegar dados do google: ', error.response)
                  Toast.show({
                    text: 'Não foi possível realizar esta ação agora',
                    position: 'bottom',
                    buttonText: 'Ok'
                  })
                  reject(error)
                }
              )
          },
          error => {
            console.log('Erro ao pegar dados do google: ', error)
            Toast.show({
              text: 'Não foi possível realizar esta ação agora',
              position: 'bottom',
              buttonText: 'Ok'
            })
            reject(error)
          }
        )
    })
  }
}

const facebookRegisterOrLogin = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(function (result) {
        if (result.isCancelled) {
          console.log('Login Cancelled')
        } else {
          console.log('Login Success')
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              fbGetUserData(data.accessToken)
                .then(
                  user => {
                    dispatch({
                      type: FACEBOOK_LOGIN,
                      payload: user
                    })
                    // login social na API do agora2030
                    let dataLogin = {
                      socialLoginOrigin: 'facebook',
                      email: user.email,
                      accessToken: data.accessToken
                    }
                    if (user.picture.data.url) {
                      dataLogin.socialAvatar = user.picture.data.url
                    }
                    socialLogin(dataLogin).then(
                      success => {
                        if (success.data.newUser) {
                          const u = {
                            email: user.email || '',
                            socialAvatar: user.picture.data.url || '',
                            firstName: user.first_name || '',
                            lastName: user.last_name || user.first_name || '',
                            socialLoginOrigin: 'facebook'
                          }
                          // dispatch(NavigationActions.navigate({ routeName: 'Register' }))
                          _socialSignUp(u, dispatch)
                            .then(
                              success => {
                                resolve(success)
                              },
                              error => {
                                reject(error)
                              }
                            )
                        } else {
                          dispatch({
                            type: MODIFICA_API_TOKEN,
                            payload: success.data.data
                          })
                          dispatch({
                            type: UPDATE_USER_PROFILE,
                            payload: success.data.data
                          })
                        }
                        resolve(success)
                      },
                      error => {
                        console.log(error)
                        Toast.show({
                          text: 'Não foi possível realizar esta ação agora',
                          position: 'bottom',
                          buttonText: 'Ok'
                        })
                        reject(error)
                      }
                    )
                    // login social na API do agora2030
                  },
                  error => {
                    console.log(error)
                    reject(error)
                  }
                )
            }
          )
        }
      }, function (error) {
        console.log('some error occurred!!')
        console.log(error)
        reject(error)
      })
    })
  }
}

export {
  modificaReceiveEmail,
  modificaReceivePush,
  doRegister,
  googleRegisterOrLogin,
  facebookRegisterOrLogin,
  updateUserProfile,
  doEditProfile
}
