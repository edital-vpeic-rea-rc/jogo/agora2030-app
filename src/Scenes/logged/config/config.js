import React, { Component } from 'react'
import { Alert, Switch, DeviceEventEmitter } from 'react-native'
import {
  Container, Content, List, ListItem, Body, Button, Left, Right
} from 'native-base'
import AppHeader from '../../../Components/app-header'
import styles from './styles'
import { connect } from 'react-redux'
import {
  updateUserProfile, doEditProfile, modificaReceivePush,
  modificaReceiveEmail
} from '../../../Redux/UserProfile/UserProfileActions'
import Text from '../../../Components/text'
import Icon from 'react-native-vector-icons/FontAwesome'

const mapStateToProps = state => (
  {
    apiToken: state.UserProfile.apiToken,
    receivePush: state.UserProfile.receivePush,
    receiveEmail: state.UserProfile.receiveEmail,
    socialLoginOrigin: state.UserProfile.socialLoginOrigin
  }
)

class Config extends Component {
  constructor (props) {
    super(props)
    this.state = {
      disableSwitchEmail: false,
      disableSwitchPush: false
    }
  }

  _logout () {
    Alert.alert(
      'Encerrar sessão',
      'Deseja realmente sair ?',
      [
        {text: 'Não', onPress: () => {}, style: 'cancel'},
        {
          text: 'Sim',
          onPress: () => {
            this.props.updateUserProfile('logout')
            DeviceEventEmitter.emit('doLogout', { expiredSession: false })
          }
        }
      ]
    )
  }

  render () {
    const { navigate } = this.props.navigation
    return (
      <Container style={styles.container}>
        <AppHeader {...this.props} title='Configurações' backButton />
        <Content style={{ marginTop: 20 }} padder>
          <List>
            <ListItem style={styles.listItem}>
              <Left>
                <Text>Receber Notificações por Email</Text>
              </Left>
              <Right>
                <Switch
                  value={this.props.receiveEmail}
                  disabled={this.state.disableSwitchEmail}
                  onValueChange={() => {
                    this.props.modificaReceiveEmail()
                    this.setState({ disableSwitchEmail: true })
                    this.props.doEditProfile({ receiveEmail: !this.props.receiveEmail })
                      .then(
                        success => this.setState({ disableSwitchEmail: false }),
                        error => {
                          this.props.modificaReceiveEmail()
                          this.setState({ disableSwitchEmail: false })
                        }
                      )
                  }}
                  />
              </Right>
            </ListItem>

            <ListItem style={styles.listItem}>
              <Left>
                <Text>Receber Notificações no smartphone</Text>
              </Left>
              <Right>
                <Switch
                  value={this.props.receivePush}
                  disabled={this.state.disableSwitchPush}
                  onValueChange={() => {
                    this.props.modificaReceivePush()
                    this.setState({ disableSwitchPush: true })
                    this.props.doEditProfile({ receivePush: !this.props.receivePush })
                      .then(
                        success => this.setState({ disableSwitchPush: false }),
                        error => {
                          this.props.modificaReceivePush()
                          this.setState({ disableSwitchPush: false })
                        }
                      )
                  }}
                  />
              </Right>
            </ListItem>
            <ListItem
              style={styles.listItem}
              onPress={() => navigate('RecoverPwd', { isReset: true })}
            >
              <Left><Text>Alterar sua Senha</Text></Left>
            </ListItem>
            <ListItem
              style={styles.listItem}
              onPress={() => {
                navigate('Register', { isEdit: true })
              }}
            >
              <Body>
                <Text>Editar Perfil</Text>
              </Body>
            </ListItem>
            <ListItem
              style={styles.listItem}
              onPress={() => navigate('Tutorial', { goBack: true })}
            >
              <Body>
                <Text>Ver tutorial Inicial novamente</Text>
              </Body>
            </ListItem>
          </List>
          <Button
            iconLeft
            block
            rounded
            danger
            onPress={() => { this._logout() }}
            style={styles.buttonLogout}
            >
            <Icon name='times' size={18} style={{ color: '#fff', marginTop: 0, marginRight: 4 }} />
            <Text style={styles.buttonTextLogout}>
              Sair
            </Text>
          </Button>
        </Content>
      </Container>
    )
  }
}

export default connect(mapStateToProps, {
  updateUserProfile,
  doEditProfile,
  modificaReceiveEmail,
  modificaReceivePush
})(Config)
