import React, { Component } from 'react'
import {
  Image, View, TouchableOpacity, RefreshControl
} from 'react-native'
import {
  Card, CardItem, Spinner, List
} from 'native-base'
import { getUserActivities } from '../../../Api/profile'
import { connect } from 'react-redux'
import Text from '../../../Components/text'
import mStyles from '../../../Assets/Styles/Styles'
import styles from './styles'
import AppHeader from '../../../Components/app-header'

const mapStateToProps = state => (
  {
    apiToken: state.UserProfile.apiToken,
    fullProfile: state.UserProfile
  }
)

class UserInitiativesList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      initiatives: [],
      showLoading: true,
      refreshing: false,
      error: false,
      errorText: '',
      page: 2,
      limitInitial: 50,
      skip: 0,
      scope: '',
      pageTitle: 'Iniciativas'
    }
  }

  componentWillMount () {
    const { apiToken } = this.props
    switch (this.props.navigation.state.params.type) {
      case 'actionFavorite':
        this.setState({ pageTitle: 'Iniciativas Favoritas' })
        break
      case 'actionDonate':
        this.setState({ pageTitle: 'Iniciativas Apoiadas' })
        break
      case 'actionView':
        this.setState({ pageTitle: 'Iniciativas Visualizadas' })
    }
    this._getInitiatives(apiToken)
  }

  refreshInitiatives () {
    this.setState({ refreshing: true })
    // const { limitInitial, scope, filter } = this.state
    const { params } = this.props.navigation.state
    getUserActivities(this.props.apiToken, params.type, this.props.fullProfile.id)
      .then(
        success => {
          let initiatives = []
          success.data.map(item => initiatives.push(item.initiative))
          this.setState({initiatives, refreshing: false})
          // console.log(success)
        },
        error => {
          console.log(error.response)
          this.setState({refreshing: false, error: true, errorText: error.response.data.message})
        }
      )
  }

  _getInitiatives (token) {
    // let { limitInitial, scope, filter } = this.state
    const { params } = this.props.navigation.state
    getUserActivities(token, params.type, this.props.fullProfile.id)
    .then(
      success => {
        let initiatives = []
        success.data.map(item => initiatives.push(item.initiative))
        this.setState({initiatives, loadingSearch: false, showLoading: false})
      },
      error => {
        console.log(error.response)
        this.setState({showLoading: false, loadingSearch: false, error: true, errorText: error.response.data.message})
      }
    )
  }

  renderRow (item) {
    return (
      <TouchableOpacity
        onPress={() => this.props.navigation.navigate('InitiativeView', {id: item.id})}
      >
        <Card style={styles.card}>
          <CardItem>
            <Image
              source={{uri: item.coverImage.urls['570x370']}}
              style={styles.thumbnail}
              />
          </CardItem>

          <CardItem>
            <Text style={styles.title}>{item.title}</Text>
          </CardItem>

          <CardItem footer>
            <View style={[ styles.row, styles.rowIcos, { flex: 1, marginTop: -5 } ]}>
              {
                item.ods.map((ods, index) => (
                  <View style={[ mStyles.ods, { backgroundColor: ods.color } ]} key={index} >
                    <Text style={mStyles.odsId}>{ods.number}</Text>
                  </View>
              ))
              }

            </View>
          </CardItem>
        </Card>
      </TouchableOpacity>)
  }

  render () {
    if (this.state.showLoading) {
      return (<View style={styles.container}><Spinner width={35} color='#666' /></View>)
    }
    if (this.state.error) {
      return (<View style={styles.container}><Text>Erro ao obter iniciativas: {this.state.errorText}</Text></View>)
    }

    return (
      <View style={{ flex: 1 }}>
        <AppHeader title={this.state.pageTitle} navigation={this.props.navigation} backButton />
        <View style={styles.container}>
          {
            this.state.initiatives.length === 0
            ? (<View style={styles.nocontent}><Text>Você não tem nenhuma Iniciativa aqui ainda.</Text></View>)
            : (
              <List
                refreshControl={
                  <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.refreshInitiatives.bind(this)}
                    title='Atualizando...'
                />
              }
                dataArray={this.state.initiatives}
                renderRow={this.renderRow.bind(this)}
              />
            )
          }
        </View>
      </View>
    )
  }
}

export default connect(mapStateToProps, null)(UserInitiativesList)
