import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e6ebee'
  },
  row: {
    flexDirection: 'row'
  },
  card: {
    borderRadius: 15,
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 15,
    padding: 10
  },
  thumbnail: {
    flex: 1,
    height: 140,
    resizeMode: 'cover',
    marginHorizontal: -3
  },
  title: {
    fontWeight: 'bold',
    lineHeight: 20
  },
  rowIcos: {
    marginTop: 0
  },
  icoView: {
    alignItems: 'flex-end',
    right: 0,
    position: 'absolute'
  },
  ico: {
    height: 30,
    width: 30
  },
  nocontent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
