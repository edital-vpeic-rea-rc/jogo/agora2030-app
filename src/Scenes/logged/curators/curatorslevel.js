import React, { Component } from 'react'
import { Dimensions, FlatList, TouchableOpacity, View } from 'react-native'
import { Thumbnail, Spinner, List, ListItem } from 'native-base'
import AppHeader from '../../../Components/app-header'
import Text from '../../../Components/text'
import styles from './styles'
import {getUsersByLevel} from '../../../Api/user'
import { connect } from 'react-redux'
import {getUserAvatar, formatDate} from '../../../helpers'

const { width, height } = Dimensions.get('window')
const widthColInfo = (width - 95)
const widthColViewProfile = 50

const mapStateToProps = state => (
  {
    apiToken: state.UserProfile.apiToken
  }
)

class CuratorsLevel extends Component {
  constructor (props) {
    super(props)
    this.state = {
      title: this.props.navigation.state.params.title,
      nivel: this.props.navigation.state.params.nivel,
      size: { width, height },
      showLoading: true,
      users: [],
      currentPage: 1,
      currentLimit: 0,
      total: 99999,
      isFetching: false
    }
  }

  componentWillMount () {
    this.getUsers()
  }

  getUsers (page = this.state.currentPage) {
    const { users, nivel, currentLimit } = this.state
    this.setState({ isFetching: true }, () => {
      getUsersByLevel(nivel, this.props.apiToken, 10, currentLimit * (page - 1))
        .then(
          success => {
            console.log(success.data)
            this.setState({
              users: [...users, ...success.data.items],
              showLoading: false,
              currentLimit: success.data.limit,
              total: success.data.total,
              isFetching: false
            })
          },
          error => {
            console.log('Erro ao pegar usuários: ', error.response)
            this.setState({ showLoading: false, isFetching: false })
          }
        )
    })
  }

  getMore () {
    const { users, total, isFetching } = this.state
    if (users.length < total && !isFetching) {
      this.setState({
        currentPage: this.state.currentPage + 1,
        showLoading: true
      }, () => {
        this.getUsers()
      })
    }
  }

  noUsersRender () {
    return (
      <View style={styles.viewNoCurators}>
        <Text style={[styles.noCurators, { fontWeight: 'bold' }]}>Ainda não existem curarores nesse nível.</Text>
        <Text style={styles.noCurators}>Continue interagindo com as iniciativas para subir de nível e seja o primeiro!</Text>
      </View>
    )
  }

  renderRow (item) {
    const usr = item.item
    return (
      <ListItem style={{backgroundColor: '#e6ebee', borderBottomWidth: 0.5, borderBottomColor: 'rgba(0,0,0,.2)'}}>
        <Thumbnail size={80} source={getUserAvatar(usr)} />
        <View style={{ marginLeft: 10, flexDirection: 'row', width: widthColInfo }}>
          <View style={{ width: (widthColInfo - widthColViewProfile) }}>
            <Text numberOfLines={2}>{usr.firstName} {usr.lastName}</Text>
            <Text note>Pontos: {usr.xp}</Text>
            <Text note>Desde: {formatDate(usr.createdAt)}</Text>
          </View>

          <View style={{ marginTop: 25, width: widthColViewProfile}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Profile', { uid: usr.id })}
            >
              <Text style={{ fontSize: 12, alignSelf: 'flex-end' }}>Ver Perfil</Text>
            </TouchableOpacity>          
          </View>
        </View>
      </ListItem>
    )
  }

  render () {
    return (
      <View style={styles.container}>
        <AppHeader {...this.props} title={this.state.title.substring(6)} backButton />

        { (this.state.users.length === 0 && !this.state.showLoading) ? this.noUsersRender() : null}
        <List>
          <FlatList
            data={this.state.users}
            renderItem={this.renderRow.bind(this)}
            keyExtractor={(item, index) => index}
            onEndReached={this.getMore.bind(this)}
            onEndReachedThreshold={1}
          />
        </List>
        {this.state.showLoading ? (<View style={styles.containerSpinner}><Spinner width={35} color='#666' /></View>) : null}

      </View>
    )
  }
}

export default connect(mapStateToProps, null)(CuratorsLevel)
