import React, { Component } from 'react'
import { View, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import Carousel from 'react-native-looped-carousel'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class ImageCarousel extends Component {
  showZoom (images, index) {
    if (this.props.zoom) {
      this.props.navigation.navigate('PhotoZoomer', { images, startAt: index })
    }
  }
  render () {
    const {
      isLoading,
      images,
      size,
      autoplay
    } = this.props
    if (isLoading) {
      return (<ActivityIndicator size={'large'} color='#666' />)
    }
    return (
      <Carousel
        autoplay={autoplay}
        style={{ width: size.width, height: 200 }}
      >
        {images.map((image, index) => {
          return (
            <View style={[{ backgroundColor: '#ccc' }, size]} key={index}>
              <TouchableOpacity
                onPress={() => this.showZoom(images, index)}
              >
                <Image
                  source={{ uri: image.urls['570x370'] }}
                  style={{width: null, height: 200}}
                >
                  <Icon
                    name='search-plus'
                    size={25}
                    style={{
                      color: 'rgba(255,255,255,.5)',
                      position: 'absolute',
                      right: 10,
                      bottom: 10,
                      backgroundColor: 'transparent',
                      display: this.props.zoom ? 'flex' : 'none'
                    }}
                    />
                </Image>
              </TouchableOpacity>
            </View>
          )
        })}
      </Carousel>
    )
  }
}
