import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { Text, Spinner, Toast, Button } from 'native-base'
import Modal from 'react-native-modal'
import H3 from '../../../../Components/h3'
import { favorite } from '../../../../Api/initiativeActions'
import Icon from 'react-native-vector-icons/FontAwesome'
import mStyles from '../../../../Assets/Styles/Styles'

export default class ActionFavorite extends Component {
  constructor (props) {
    super(props)
    this.state = {
      actionFavorite: this.props.actionFavorite,
      actionFavoriteShowModal: false,
      actionFavoriteModalConfirmLoading: false,
      favorites: this.props.favorites,
      actionFavoriteDisbleModalBtnConfirm: false
    }
  }

  _buttonRender () {
    if (this.state.favorites <= 0 && !this.state.actionFavorite) {
      return (
        <TouchableOpacity onPress={() => this.setState({ actionFavoriteShowModal: true })}>
          <Image
            style={{height: 50, width: 50}}
            source={require('../../../../Assets/Images/actions/actionFavorite_blocked.png')}
          />
        </TouchableOpacity>
      )
    } else if (this.state.actionFavorite) {
      return (
        <Image
          style={{height: 50, width: 50}}
          source={require('../../../../Assets/Images/actions/actionFavorite.png')}
        />
      )
    } else {
      return (
        <TouchableOpacity onPress={() => this.setState({ actionFavoriteShowModal: true })}>
          <Image
            style={{height: 50, width: 50}}
            source={require('../../../../Assets/Images/actions/actionFavorite_disabled.png')}
          />
        </TouchableOpacity>
      )
    }
  }

  _confirm () {
    this.setState({actionFavoriteDisbleModalBtnConfirm: true})
    favorite(this.props.initiativeId, this.props.apiToken)
    .then(
      success => {
        this.setState({
          actionFavorite: true,
          actionFavoriteModalConfirmLoading: false,
          actionFavoriteShowModal: false,
          actionFavoriteDisbleModalBtnConfirm: false,
          favorites: this.state.favorites - 1
        })
      },
      error => {
        console.log('Erro ao favoritar: ', error, this.props.initiativeId)
        this.setState({
          actionFavoriteModalConfirmLoading: false,
          actionFavoriteDisbleModalBtnConfirm: false,
          actionFavoriteShowModal: false
        })
        Toast.show({
          text: 'Houve um erro na ação. =|',
          position: 'bottom',
          buttonText: 'Ok',
          duration: 4000
        })
      }
    )
  }

  _modalContent () {
    if (this.state.favorites === 0) {
      return (
        <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
          <H3 style={{textAlign: 'center'}}>Favoritar</H3>
          <Text style={{textAlign: 'center'}}>
            Você não possui favoritos disponíveis. =|
          </Text>
          <Button transparent full onPress={() => { this.setState({ actionFavoriteShowModal: false }) }}>
            <Text>Ok</Text>
          </Button>
        </View>
      )
    } else {
      return (
        <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
          <Button
            transparent
            small
            style={mStyles.modalBtClose}
            onPress={() => { this.setState({ actionFavoriteShowModal: false }) }}
            >
            <Icon name='times' size={20} style={mStyles.modalIcoClose} />
          </Button>

          <H3 style={{textAlign: 'center'}}>Favoritar</H3>
          {this.state.actionFavoriteModalConfirmLoading ? <Spinner width={35} color='#666' /> : null}
          <Text style={{textAlign: 'center'}}>
            Você tem {this.state.favorites} favoritos disponíveis.
          </Text>
          <Text style={{ marginVertical: 10 }}>
            Ao adicionar essa Iniciativa como favorita, essa ação não poderá ser desfeita.
          </Text>

          <View style={{ alignSelf: 'center' }}>
            <Button
              rounded
              onPress={() => {
                this.setState({ actionFavoriteModalConfirmLoading: true })
                this._confirm()
              }}
              disabled={this.state.actionFavoriteDisbleModalBtnConfirm}
              style={mStyles.buttonPrimary}
              >
              <Text style={[mStyles.textButtonPrimary, {textAlign: 'center', marginTop: 4, width: 170}]}>Confirmar</Text>
            </Button>
          </View>
        </View>
      )
    }
  }

  render () {
    return (
      <View>
        {this._buttonRender()}
        <Modal
          isVisible={this.state.actionFavoriteShowModal}
          onBackdropPress={() => { this.setState({actionFavoriteShowModal: false}) }}
          style={{ alignSelf: 'center', margin: 30, width: 250 }}
         >
          {this._modalContent()}
        </Modal>
      </View>
    )
  }
}
