import React, { Component } from 'react'
import { View } from 'react-native'
import styles from '../styles'
import Line from '../../../../Components/line'
import H2 from '../../../../Components/h2'
import Text from '../../../../Components/text'
import Ods from './ods'

export default class Informations extends Component {
  _renderSource () {
    return (
      <View>
        <View style={{marginBottom: 10}}>
          <Text style={{ fontWeight: 'bold' }}>Fonte</Text>
          <Text>{this.props.initiative.source}</Text>
        </View>
      </View>
    )
  }

  render () {
    const {initiative} = this.props
    return (
      <View style={styles.contentMargin}>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center', marginTop: 20}}>
          {
            initiative.tags.map((item, index) => {
              return (
                <View style={styles.tag} key={index}>
                  <Text style={{ fontSize: 12, color: '#fff' }}>{item.text}</Text>
                </View>
              )
            })
          }
        </View>
        <Line />
        <View>
          <H2 style={styles.title}>{initiative.title}</H2>
        </View>
        <Ods
          ods={initiative.ods}
        />
        <View>
          <View style={{marginBottom: 10}}>
            <Text style={{ fontWeight: 'bold' }}>Local</Text>
            <Text>{initiative.addressCity} / {initiative.addressState}</Text>
          </View>

          <View style={{marginBottom: 10}}>
            <Text style={{ fontWeight: 'bold' }}>Responsável</Text>
            <Text>{initiative.institutionName}</Text>
          </View>

          {(initiative.source) ? this._renderSource() : null }
        </View>
        <View style={{marginBottom: 15}}>
          <Text style={{ fontWeight: 'bold' }}>Resumo</Text>
          <Text>{initiative.description}</Text>
        </View>
      </View>
    )
  }
}
