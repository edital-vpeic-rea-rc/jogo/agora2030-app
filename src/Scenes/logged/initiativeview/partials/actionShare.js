import React, { Component } from 'react'
import { View, Image, TouchableOpacity, Share, Platform } from 'react-native'
import { Toast } from 'native-base'
import { share } from '../../../../Api/initiativeActions'
import { pageHost } from '../../../../Api/api.variables'

export default class ActionShare extends Component {
  constructor (props) {
    super(props)
    this.state = {
      actionShare: this.props.actionShare
    }
  }

  _buttonRender () {
    return (
      <TouchableOpacity onPress={() => this._confirm()}>
        <Image
          style={{height: 50, width: 50}}
          source={this.state.actionShare
          ? require('../../../../Assets/Images/actions/actionShare.png')
          : require('../../../../Assets/Images/actions/actionShare_disabled.png')
        }
        />
      </TouchableOpacity>
    )
  }

  _confirm () {
    const platform = Platform.OS

    let content = {
      title: this.props.title,
      message: this.props.title,
      url: `${pageHost}/initiative/${this.props.slug}`
    }

    content.message = (platform === 'ios') ? `${content.message}.` : `${content.message}: ${content.url}`

    Share.share(content)
    .then(
      success => {
        if (!this.state.actionShare) {
          share(this.props.initiativeId, this.props.apiToken)
          .then(
            success => {
              this.setState({actionShare: true})
            },
            error => {
              console.log('Erro ao compartilhar: ', error, this.props.initiativeId)
              Toast.show({
                text: 'Houve um erro na ação. =|',
                position: 'bottom',
                buttonText: 'Ok',
                duration: 4000
              })
            }
          )
        }
      }
    )
  }

  render () {
    return (
      <View>
        {this._buttonRender()}
      </View>
    )
  }
}
