import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'

import RowIcoText from '../../../../Components/row-ico-text'
import styles from '../styles'

export default class ActivitiesList extends Component {
  renderRow (item, index) {
    switch (item.type) {
      case 'actionView':
        return (
          <RowIcoText
            key={index}
            ico={require(`../../../../Assets/Images/actions/actionView.png`)}
            text={`Visualizou ${item.initiative.title}`}
          />
        )
      case 'actionLike':
        return (
          <RowIcoText
            key={index}
            ico={require(`../../../../Assets/Images/actions/actionLike.png`)}
            text={`Curtiu ${item.initiative.title}`}
          />
        )
      case 'actionFavorite':
        return (
          <RowIcoText
            key={index}
            ico={require(`../../../../Assets/Images/actions/actionFavorite.png`)}
            text={`Favoritou ${item.initiative.title}`}
          />
        )
      case 'actionComment':
        return (
          <RowIcoText
            key={index}
            ico={require(`../../../../Assets/Images/actions/actionComment.png`)}
            text={`Comentou ${item.initiative.title}`}
          />
        )
      case 'actionShare':
        return (
          <RowIcoText
            key={index}
            ico={require(`../../../../Assets/Images/actions/actionShare.png`)}
            text={`Compartilhou ${item.initiative.title}`}
          />
        )
      case 'actionDonate':
        return (
          <RowIcoText
            key={index}
            ico={require(`../../../../Assets/Images/actions/actionDonate.png`)}
            text={`Apoiou ${item.initiative.title}`}
          />
        )
    }
  }
  render () {
    if (this.props.isLoading) {
      return (<ActivityIndicator size={'large'} color='#666' />)
    }
    return (
      <View style={styles.mainContent}>
        {this.props.activities.map((item, index) => this.renderRow(item, index))}
      </View>
    )
  }
}
