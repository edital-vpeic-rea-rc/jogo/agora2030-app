import { Platform, StyleSheet } from 'react-native'

export default StyleSheet.create({

  row: {
    flexDirection: 'row'
  },
  headerBar: {
    justifyContent: 'space-between',
    ...Platform.select({
      ios: {
        marginHorizontal: 20,
        marginTop: 40
      },
      android: {
        paddingTop: 20,
        paddingHorizontal: 10,
        paddingRight: 18,
        flex: 1
      }
    })
  },
  backButton: {
    ...Platform.select({
      android: {
        marginTop: 10
      }
    })
  },
  mainHeader: {
    backgroundColor: '#fff',
    borderColor: 'transparent',
    borderWidth: 0,
    height: 80,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  header: {
    backgroundColor: '#dae4ea',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    // height: 160,
    marginBottom: 20,
    paddingTop: 40
    // ...Platform.select({
    //   ios: {
    //     marginTop: 30
    //   }
    // })
  },
  colThumbnail: {
    flex: 1,
    justifyContent: 'center'
  },
  avatar: {
    borderRadius: 55,
    borderWidth: 5,
    alignSelf: 'center',
    width: 110,
    height: 110,
    marginLeft: 10
  },
  _badge: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 5
  },
  colInfo: {
    flex: 2,
    justifyContent: 'center',
    marginLeft: 20
  },
  nameUser: {
    color: '#595d64',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5
  },
  text: {
    fontSize: 14
  },
  levelNane: {
    color: '#595d64',
    fontSize: 10,
    marginTop: 5,
    marginBottom: 3
  },
  editProfile: {
    color: '#595d64',
    textDecorationLine: 'underline',
    fontSize: 10,
    alignSelf: 'flex-end',
    marginTop: 10,
    marginRight: 10,
    paddingBottom: 3
  },
  stamps: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: '#b5d7ec',
    // height: 100,
    paddingTop: 50,
    position: 'absolute',
    top: 150,
    left: 0,
    right: 0,
    zIndex: -1

  },
  filter: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: '#ededed',
    paddingTop: 55,
    position: 'absolute',
    top: 240,
    left: 0,
    right: 0,
    zIndex: -2
  },
  colLabel: {
    flex: 2
  },
  colActivities: {
    flex: 3
  },
  picker: {
    backgroundColor: 'transparent',
    borderColor: '#636363',
    borderRadius: 20,
    borderWidth: 3,
    height: 35,
    marginTop: -5
  },
  mainContent: {
    backgroundColor: '#f3f3f3',
    marginTop: 150,
    paddingTop: 20,
    paddingBottom: 30,
    ...Platform.select({
      android: {
        marginBottom: 60
      }
    })
  },
  listItem: {
    backgroundColor: 'transparent'
  }
})
