import React, { Component } from 'react'
import { AsyncStorage, View, DeviceEventEmitter } from 'react-native'
import { connect } from 'react-redux'

import LoginScene from '../login/login'
import TutorialScene from '../carouselstart/carouselstart'
import DashboardScene from '../dashboard/dashboard'
import Loader from '../../Components/loader'

class Scene extends Component {
  constructor (props) {
    super(props)
    this.state = {
      screens: [
        (<LoginScene {...props} />),
        (<TutorialScene {...props} />),
        (<DashboardScene {...props} />)
      ],
      active: 0,
      showLoader: true
    }
    AsyncStorage.multiGet(['apiToken', 'showTutorial'], (err, data) => {
      if (err) {
        console.log('erro', err)
      }
      let keys = {}
      data.map(item => { keys[item[0]] = item[1] })
      if (keys['apiToken'] && keys['apiToken'].length > 0) {
        if (keys['showTutorial'] && keys['showTutorial'] === 'false') {
          this.setState({ active: 2 }) // Dashboard ativa
        } else {
          this.setState({ active: 1 }) // tutorial ativo
        }
        DeviceEventEmitter.emit('initPersistence', { source: 'initial.js', repopulate: true })
      }
      this.setState({ showLoader: false })
    })
  }
  render () {
    return (
      <View style={{ flex: 1 }}>
        <Loader visible={this.state.showLoader} />
        {this.state.screens[this.state.active]}
      </View>
    )
  }
}

export default connect(null, null)(Scene)
