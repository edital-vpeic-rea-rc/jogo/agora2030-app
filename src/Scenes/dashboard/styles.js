import { StyleSheet } from 'react-native'


export default StyleSheet.create({
  tab: {
    backgroundColor: '#fff'
  }, 
  activeTab: {
    backgroundColor: '#4ba146', 
    height: 50,
    zIndex: -1 
  }
})