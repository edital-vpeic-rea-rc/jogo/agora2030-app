import { StyleSheet, Platform } from 'react-native'

export default StyleSheet.create({
  backgroundImage: {
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 1,
    ...Platform.select({
      ios: {
        left: 0,
        top: 0
      },
      android: {
        top: -10,
        left: 0,
        right: 0
      }
    })
  },
  p: {
    // backgroundColor: 'rgba(255,255,255,.5)',
    backgroundColor: 'transparent',
    textAlign: 'justify',
    marginBottom: 10
    // padding: 2
  },
  textCompletedMission: {
    backgroundColor: 'transparent',
    textAlign: 'justify',
    marginBottom: 5,
    fontWeight: 'bold'
  },
  icon: {
    width: 90,
    height: 90,
    marginTop: -25,
    marginBottom: 5,
    alignSelf: 'center'
  },
  iconCompleted: {
    width: 50,
    height: 50,
    marginTop: 5,
    marginBottom: 5,
    alignSelf: 'center'
  },
  title: {
    backgroundColor: 'transparent',
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 10
  },
  tableHeader: {
    height: 40
  },
  tableRow: {
    textAlign: 'center',
    padding: 4
  },
  backgroundImage: {
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 0,
    ...Platform.select({
      ios: {
        left: 0,
        top: 0
      },
      android: {
        top: -10,
        left: 0,
        right: 0
      }
    })
  }
})
