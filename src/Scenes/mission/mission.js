import React, { Component } from 'react'
import {ScrollView, Image, ImageBackground, Dimensions, TouchableOpacity} from 'react-native'
import styles from './styles'
import {View, Container, Toast, Button, Spinner} from 'native-base'
import {getMissionIcon, getActionIcon, getMissionBg} from '../../helpers'
import Text from '../../Components/text'
import H3 from '../../Components/h3'
import { Table, Row } from 'react-native-table-component'
import AppHeader from '../../Components/app-header'
import Line from '../../Components/line'
import I18n from '../../Locales'
import Modal from 'react-native-modal'
import mStyles from '../../Assets/Styles/Styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import {sendCertificate} from '../../Api/user'
import ShareWidget from '../../Components/share-widget'

export default class Mission extends Component {
  constructor (props) {
    super(props)
    this.state = {
      certificateShowModal: false,
      certificateModalConfirmLoading: false,
      certificateDisbleModalBtnConfirm: false
    }
  }
  _renderActions (mission, tracestats) {
    let data = []
    if (mission.actionView) {
      data.push({
        icon: 'actionView',
        qnt: mission.actionView,
        actual: tracestats.actionView,
        status: (tracestats.actionView >= mission.actionView) ? 'Ok' : 'X'
      })
    }
    if (mission.actionLike) {
      data.push({
        icon: 'actionLike',
        qnt: mission.actionLike,
        actual: tracestats.actionLike,
        status: (tracestats.actionLike >= mission.actionLike) ? 'Ok' : 'X'
      })
    }
    if (mission.actionFavorite) {
      data.push({
        icon: 'actionFavorite',
        qnt: mission.actionFavorite,
        actual: tracestats.actionFavorite,
        status: (tracestats.actionFavorite >= mission.actionFavorite) ? 'Ok' : 'X'
      })
    }
    if (mission.actionShare) {
      data.push({
        icon: 'actionShare',
        qnt: mission.actionShare,
        actual: tracestats.actionShare,
        status: (tracestats.actionShare >= mission.actionShare) ? 'Ok' : 'X'
      })
    }
    if (mission.actionComment) {
      data.push({
        icon: 'actionComment',
        qnt: mission.actionComment,
        actual: tracestats.actionComment,
        status: (tracestats.actionComment >= mission.actionComment) ? 'Ok' : 'X'
      })
    }
    if (mission.actionDonate) {
      data.push({
        icon: 'actionDonate',
        qnt: mission.actionDonate,
        actual: tracestats.actionDonate,
        status: (tracestats.actionDonate >= mission.actionDonate) ? 'Ok' : 'X'
      })
    }
    return (
      <View>
        <ScrollView>
          <Table style={{flex: 1}} borderStyle={{borderWidth: 0.5, borderColor: '#c8e1ff'}}>
            <Row data={['Ação', 'Necessário', 'Atual', 'Status']}
              style={styles.tableHeader}
              textStyle={{textAlign: 'center'}} flexArr={[1, 2, 1, 2]} />
            {
            data.map((item, index) => (
              <Row
                key={index}
                textStyle={styles.tableRow} flexArr={[1, 2, 1, 2]}
                data={[
                  <Image source={getActionIcon(item.icon)} style={{marginHorizontal: 5, width: 35, height: 35}} />,
                  item.qnt,
                  item.actual,
                  item.status]}
              />
            ))
          }
          </Table>
        </ScrollView>
      </View>
    )
  }

  _certificateButtonAndShare (missionName) {
    if (missionName === 'Curadoria') {
      return (
        <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 1, alignItems: 'center', flexDirection: 'row'}}>
            <ShareWidget
              activeStyle
              height={60}
              width={60}
              message={'Ganhei um novo selo na curadoria do Desafio Ágora 2030! Participe também. =)'}
              url={`https://agora.fiocruz.br/selo/?s=${missionName.toLowerCase()}`}
            />
            <Text style={{ marginLeft: 5, backgroundColor: 'transparent' }}>Compartilhar</Text>
          </View>
          <TouchableOpacity onPress={() => { this.setState({ certificateShowModal: true }) }}>
            <View style={{flex: 1, alignItems: 'center', flexDirection: 'row', marginLeft: 20}}>
              <Image style={{ width: 60, height: 60 }} source={getMissionIcon(missionName)} />
              <Text style={{ marginLeft: 5, backgroundColor: 'transparent' }}>Certificado</Text>
            </View>
          </TouchableOpacity>
        </View>
      )
    }
    return (
      <View style={{flex: 1, alignItems: 'flex-start', marginTop: 5}}>
        <ShareWidget
          activeStyle
          height={60}
          width={60}
          message={'Ganhei um novo selo na curadoria do Desafio Ágora 2030! Participe também. =)'}
          url={`https://agora.fiocruz.br/selo/?s=${missionName.toLowerCase()}`}
        />
      </View>
    )
  }

  _certificateModelContent () {
    return (
      <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
        <Button
          transparent
          small
          style={mStyles.modalBtClose}
          onPress={() => { this.setState({ certificateShowModal: false }) }}
          >
          <Icon name='times' size={20} style={mStyles.modalIcoClose} />
        </Button>

        <H3 style={{textAlign: 'center'}}>Certificado</H3>

        {this.state.certificateModalConfirmLoading ? <Spinner width={35} color='#666' /> : null}

        <Text style={{ marginVertical: 10 }}>
          Toque em 'Gerar certificado' e o seu certificado de curador será enviado para o seu email de cadastro.
        </Text>

        <View style={{ alignSelf: 'center' }}>
          <Button
            onPress={() => {
              this.setState({ certificateModalConfirmLoading: true })
              this._sendCertificate()
            }}
            disabled={this.state.certificateDisbleModalBtnConfirm}
            style={mStyles.buttonPrimary}>
            <Text style={[mStyles.textButtonPrimary, { textAlign: 'center', backgroundColor: 'transparent', width: 200, marginTop: 0 }]}>Gerar certificado</Text>
          </Button>
        </View>
      </View>

    )
  }

  _sendCertificate () {
    this.setState({certificateDisbleModalBtnConfirm: true})
    sendCertificate()
    .then(
      success => {
        this.setState({
          certificateModalConfirmLoading: false,
          certificateShowModal: false,
          certificateDisbleModalBtnConfirm: false
        })
        Toast.show({
          text: 'Certificado Enviado! =)',
          position: 'bottom',
          buttonText: 'Ok',
          duration: 4000
        })
      },
      error => {
        console.log('Erro ao enviar certificado: ', error)
        this.setState({
          certificateModalConfirmLoading: false,
          certificateDisbleModalBtnConfirm: false,
          certificateShowModal: false
        })
        Toast.show({
          text: 'Houve um erro ao enviar. =|',
          position: 'bottom',
          buttonText: 'Ok',
          duration: 4000
        })
      }
    )
  }

  _completedMission () {
    const {mission} = this.props.navigation.state.params
    const title = mission.title.toLowerCase()
    return (
      <View style={{ padding: 20 }}>
        <ImageBackground
          source={getMissionBg(mission.title)}
          style={[styles.backgroundImage, {height: Dimensions.get('window').height, width: Dimensions.get('window').width}]}
        />
        <ScrollView style={{ height: Dimensions.get('window').height, marginTop: -20 }}>
          <Image
            source={getMissionIcon(mission.title)}
            style={styles.iconCompleted}
            />
          <H3 style={styles.title}>
            {I18n.t(`mission.${title}_completed.title`)}
          </H3>

          <Text style={styles.textCompletedMission}>{I18n.t(`mission.${title}_completed.intro`)}</Text>

          {/* <Text style={styles.textCompletedMission}>{I18n.t(`mission.${title}_completed.text`)}</Text> */}

          {<Text style={styles.textCompletedMission}>{I18n.t(`mission.${title}_completed.text1`)}</Text>}

          <Text style={styles.textCompletedMission}>Compartilhe! =)</Text>

          {this._certificateButtonAndShare(mission.title)}

          <Modal
            isVisible={this.state.certificateShowModal}
            onBackdropPress={() => { this.setState({certificateShowModal: false}) }}
            style={{ alignSelf: 'center', margin: 30, width: 250 }}
          >
            {this._certificateModelContent()}
          </Modal>
        </ScrollView>
      </View>
    )
  }

  _renderMission () {
    const {mission, tracestats, completed} = this.props.navigation.state.params
    if (completed) {
      return this._completedMission()
    } else {
      return (
        <View style={{ paddingVertical: 30, paddingHorizontal: 20 }}>
          <Image
            source={getMissionIcon(mission.title)}
            style={styles.icon}
              />
          <H3 style={styles.title}>
            {mission.title}
          </H3>

          <Text style={styles.p}>{mission.description}</Text>
          <Line />
          <Text style={[styles.p, {fontWeight: 'bold', textAlign: 'center'}]}>Ações de curadoria necessárias</Text>
          {this._renderActions(mission, tracestats)}
          <Text style={{fontWeight: 'bold', textAlign: 'center'}}>
            Volte e toque em iniciativas para continuar
          </Text>
        </View>
      )
    }
  }

  render () {
    return (
      <Container style={{backgroundColor: '#e6ebee'}}>
        <AppHeader {...this.props} title='Missão' backButton />
        <ScrollView>
          {this._renderMission()}
        </ScrollView>
      </Container>
    )
  }
}
