import React, { Component } from 'react'
import { View, ActivityIndicator } from 'react-native'
import PhotoView from 'react-native-photo-view'
import AppHeader from '../../Components/app-header'
import Carousel from 'react-native-looped-carousel'

export default class PhotoZoomer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true
    }
  }
  returnPhotoUri (item) {
    return { uri: item.urls['original'] }
  }
  render () {
    const { params } = this.props.navigation.state
    const { isLoading } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0, 1)' }}>
        <AppHeader
          {...this.props}
          title='Galeria'
          backButton
        />

        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            display: isLoading ? 'flex' : 'none'
          }}
        >
          <ActivityIndicator size={'large'} />
        </View>

        <Carousel
          bullets
          bulletStyle={{ backgroundColor: '#fff', borderColor: '#52a34d', borderWidth: 1 }}
          chosenBulletStyle={{ backgroundColor: '#52a34d', borderColor: '#52a34d', borderWidth: 2 }}
          autoplay={false}
          style={[ { flex: 1, backgroundColor: 'transparent' }, this.state.size ]}
          currentPage={params.startAt ? params.startAt : 0}
        >
          {params.images.map((item, index) => {
            return (
              <PhotoView
                source={this.returnPhotoUri(item)}
                minimumZoomScale={1}
                maximumZoomScale={3}
                androidScaleType='fitCenter'
                onLoad={() => {
                  if (params.startAt === index) {
                    console.log('loaded')
                    this.setState({ isLoading: false })
                  }
                }}
                style={{flex: 1}}
                key={index}
              />
            )
          })}
        </Carousel>
      </View>
    )
  }
}
