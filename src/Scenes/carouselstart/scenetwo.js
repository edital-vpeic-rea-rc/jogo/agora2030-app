import React, { Component } from 'react'
import { Image, View, ScrollView } from 'react-native'
import styles from './styles'
import I18n from '../../Locales'
import Text from '../../Components/text'
import H2 from '../../Components/h2'

export default class SceneTwo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      modalPeoples: false
    }
  }

  _ModalVisible (opts) {
    this.setState(opts)
  }

  render () {
    return (
      <View style={styles.container}>
        <ScrollView style={{padding: 10}}>
          <Image
            source={require('../../Assets/Images/tutorial_screen_03_header.png')}
            style={[ styles.header, {height: 170} ]}
            />

          <H2 style={styles.title}>{ I18n.t('tutorial.two.title') }</H2>
          <Text style={styles.p}>{ I18n.t('tutorial.two.text') }</Text>
          <Text style={styles.p}>{ I18n.t('tutorial.two.text1') }</Text>

          <View style={[styles.rowIcos, { marginTop: 5 }]}>
            <View>
              <Image
                source={require('../../Assets/Images/missions/pessoas.png')}
                style={styles.ico}
              />
              <Text style={styles.icoText}>Pessoas</Text>
            </View>
            <View>
              <Image
                source={require('../../Assets/Images/missions/planeta.png')}
                style={styles.ico}
                />
              <Text style={styles.icoText}>Planeta</Text>
            </View>
            <View>
              <Image
                source={require('../../Assets/Images/missions/prosperidade.png')}
                style={styles.ico}
                />
              <Text style={styles.icoText}>Prosperidade</Text>
            </View>
          </View>

          <View style={styles.rowIcos}>
            <View>
              <Image
                source={require('../../Assets/Images/missions/parceria.png')}
                style={styles.ico}
                />
              <Text style={styles.icoText}>Parceria</Text>
            </View>
            <View>
              <Image
                source={require('../../Assets/Images/missions/paz.png')}
                style={styles.ico}
                />
              <Text style={styles.icoText}>Paz</Text>
            </View>
            <View>
              <Image
                source={require('../../Assets/Images/missions/curadoria.png')}
                style={styles.ico}
                />
              <Text style={styles.icoText}>Curadoria</Text>
            </View>
          </View>

        </ScrollView>
      </View>
    )
  }
}
