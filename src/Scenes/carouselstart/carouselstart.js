import React, { Component } from 'react'
import { View, Dimensions } from 'react-native'
import Carousel from 'react-native-looped-carousel'
import SceneOne from './sceneone'
import SceneOne1 from './sceneone1'
import SceneTwo from './scenetwo'
import SceneThree from './scenethree'
import SceneFour from './scenefour'
import ODS from '../../Components/ods'
const { width, height } = Dimensions.get('window')

export default class CarouselStart extends Component {
  constructor (props) {
    super(props)
    this.state = {
      size: {
        width,
        height
      }
    }
  }

  render () {
    const { params } = this.props.navigation.state
    let back
    params && params.goBack ? back = true : back = false
    return (
      <View style={{ flex: 1, height, width }}>
        <Carousel
          bullets
          bulletStyle={{ backgroundColor: '#fff', borderColor: '#52a34d', borderWidth: 1 }}
          chosenBulletStyle={{ backgroundColor: '#52a34d', borderColor: '#52a34d', borderWidth: 2 }}
          autoplay={false}
          style={[ { flex: 1, backgroundColor: 'transparent' }, this.state.size ]}
        >
          <SceneOne style={this.state.size} />
          <SceneOne1 style={this.state.size} />
          <SceneTwo style={this.state.size} />
          <SceneThree style={this.state.size} />
          <SceneFour style={this.state.size} navigation={this.props.navigation} goBack={back} />
        </Carousel>

        <ODS />
      </View>
    )
  }
}
