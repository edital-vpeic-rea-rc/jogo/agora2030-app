import React, { Component } from 'react'
import { Image, StatusBar, View, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import styles from './styles'
import I18n from '../../Locales'
import Text from '../../Components/text'
import H2 from '../../Components/h2'

const mapStateToProps = state => (
  {
    firstName: state.UserProfile.firstName
  }
)

class SceneOne extends Component {
  render () {
    return (
      <View style={{ flex: 1, paddingHorizontal: 10 }}>
        <StatusBar hidden />
        <ScrollView style={{padding: 10}}>
          <Image
            source={require('../../Assets/Images/tutorial_screen_01_header.png')}
            style={[ styles.header, {height: 170} ]}
          />
          <View style={{ padding: 10 }}>
            <H2 style={styles.title}>{I18n.t('tutorial.one.title')} {this.props.firstName}!</H2>
            <Text style={styles.p}>{ I18n.t('tutorial.one.intro')}</Text>
            <Text style={styles.p}>{ I18n.t('tutorial.one.text')}</Text>
            <Text style={styles.p}>{ I18n.t('tutorial.one.text1')}</Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps, null)(SceneOne)
