import React, { Component } from 'react'
import { Container, Content, Button } from 'native-base'
import { connect } from 'react-redux'
import { Image } from 'react-native'
import { NavigationActions } from 'react-navigation'
import styles from './styles'
import mainStyles from '../../Assets/Styles/Styles'
import { modificaTutorial } from '../../Redux/Appconfig/AppconfigActions'
import I18n from '../../Locales'
import Text from '../../Components/text'
import H2 from '../../Components/h2'

class SceneFour extends Component {
  goToDashboard () {
    if (this.props.goBack) {
      this.props.navigation.goBack()
    } else {
      this.props.modificaTutorial()
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({routeName: 'Dashboard'})
        ]
      })
      this.props.navigation.dispatch(resetAction)
    }
  }

  render () {
    return (
      <Container style={styles.container}>
        <Content padder>
          <Image
            source={require('../../Assets/Images/tutorial_screen_04_certificate_header_.png')}
            style={[ styles.fullImage, {height: 270} ]}
            />

          <H2 style={styles.title}>{ I18n.t('tutorial.four.title') }</H2>
          <Text style={styles.p}>{ I18n.t('tutorial.four.text') }</Text>
          <Text style={styles.p}>{ I18n.t('tutorial.four.text1') }</Text>
          <Button
            rounded
            block
            style={mainStyles.buttonPrimary}
            onPress={this.goToDashboard.bind(this)}
          >
            <Text style={[mainStyles.textButtonPrimary, { marginTop: 1, paddingTop: 0}]}>{ I18n.t('tutorial.four.button') }</Text>
          </Button>
        </Content>
      </Container>
    )
  }
}

export default connect(
  null,
  {
    modificaTutorial
  }
)(SceneFour)
