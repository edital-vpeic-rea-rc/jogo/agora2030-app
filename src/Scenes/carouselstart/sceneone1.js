import React, { Component } from 'react'
import { Image, View, ScrollView } from 'react-native'
import styles from './styles'
import I18n from '../../Locales'
import Text from '../../Components/text'

export default class SceneOne1 extends Component {
  render () {
    return (
      <View style={styles.container}>
        <ScrollView style={{padding: 10}}>
          <Image
            source={require('../../Assets/Images/tutorial_screen_01_header.png')}
            style={[ styles.header, {height: 170} ]}
          />

          <View padder>
            <Text style={styles.p}>{ I18n.t('tutorial.one1.text')}</Text>
            <Text style={styles.p}>{ I18n.t('tutorial.one1.text1')}</Text>
            <Text style={styles.p}>{ I18n.t('tutorial.one1.text2')}</Text>
            <Text style={styles.p}>{ I18n.t('tutorial.one1.text3')}</Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}
