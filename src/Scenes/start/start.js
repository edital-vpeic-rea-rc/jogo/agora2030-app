import React, { Component } from 'react'
import { ImageBackground, Image, DeviceEventEmitter,
  Dimensions, TouchableOpacity, View, ScrollView,
  ActivityIndicator } from 'react-native'
import PhotoView from 'react-native-photo-view'
import Icon from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import * as Animatable from 'react-native-animatable'
import { getTracestatsAction } from '../../Redux/Tracestats/TracestatsActions'
import styles from './styles'
import UserHeader from '../../Components/user-header'
import UserInfoBox from '../../Components/user-infobox'
import Text from '../../Components/text'
import { getProfile } from '../../Api/profile'
import { getMissions } from '../../Api/home'
import { getMissionIcon, getMissionBg, getMapFull } from '../../helpers'
import IcoText from '../../Components/ico-text'
import { updateUserProfile } from '../../Redux/UserProfile/UserProfileActions'

const mapStateToProps = state => (
  {
    tracestats: state.Tracestats,
    apiToken: state.UserProfile.apiToken,
    fullProfile: state.UserProfile
  }
)

class Start extends Component {
  constructor (props) {
    super(props)
    console.log(props)
    this.state = {
      isModalVisible: false,
      showLoading: true,
      missionsLoading: true,
      completedMissions: [],
      missions: [],
      missionsActual: '',
      showZoomer: false,
      size: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
      }
    }
  }

  componentDidMount () {
    const { getTracestatsAction, apiToken } = this.props
    getTracestatsAction(apiToken)
    this.getAllMissions(apiToken)
    DeviceEventEmitter.addListener('updateTracestats', e => {
      this.setState({showLoading: true})
      getTracestatsAction(apiToken)
      this.getAllMissions(apiToken)
    })
  }

  getAllMissions (apiToken) {
    this.setState({ missionsLoading: true })
    getProfile('me', apiToken)
    .then(
      success => {
        const mActual = success.data.missions[success.data.missions.length - 1]
        const completed = success.data.missions.map(item => item.title)
        this.setState({
          completedMissions: completed,
          missionsActual: (mActual) ? mActual.title : ''
        })
        this.props.updateUserProfile(success.data)
        getMissions(apiToken)
        .then(
          success => {
            this.setState({missionsLoading: false, missions: success.data.items, showLoading: false})
          }
        )
      },
      error => {
        console.log('Error on get missions: ', error)
        // Provavelmente token não válido. Precisa ajustar essa validação
        if (error.response.status === 401) {
          DeviceEventEmitter.emit('doLogout', { expiredSession: true })
        }
      })
  }

  _renderMissionsIcon () {
    const { navigate } = this.props.screenProps.rootNav
    return (
      <View style={styles.rowStamps}>
        {
          this.state.missions.map((item, index) => {
            if (this.state.completedMissions.indexOf(item.title) !== -1) {
              return (
                <View key={index}>
                  <TouchableOpacity onPress={() => {
                    navigate('Mission', {mission: item, completed: true})
                  }}>
                    <IcoText source={getMissionIcon(item.title)} text={item.title} key={index} />
                  </TouchableOpacity>
                </View>
              )
            } else {
              return (
                <View key={index}>
                  <TouchableOpacity onPress={() => {
                    navigate('Mission', {mission: item, tracestats: this.props.tracestats})
                  }}>
                    <IcoText source={getMissionIcon(item.title + '_disabled')} text={item.title} />
                  </TouchableOpacity>
                </View>
              )
            }
          })
        }
      </View>
    )
  }

  _renderLogoPovoado () {
    let imgStyle = {
      width: null, resizeMode: 'contain', marginHorizontal: 40
    }
    if (this.state.size.height <= 568) {
      imgStyle.marginTop = -10
    } else {
      imgStyle.marginVertical = 10
    }
    return (
      <Image
        source={require('../../Assets/Images/missions/soglan_povoado.png')}
        style={imgStyle}
      />
    )
  }

  render () {
    if (this.state.showLoading) {
      return (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <ActivityIndicator size={'large'} color='#666' />
        </View>
      )
    }
    return (
      <View style={styles.mainView}>
        <ImageBackground
          source={getMissionBg(this.state.missionsActual)}
          style={[styles.backgroundImage, { height: this.state.size.height, width: this.state.size.width }]}
      />

        <View style={styles.userHeader}>
          <View>
            <UserHeader
              {...this.props}
              navigation={this.props.screenProps.rootNav}
              profile={this.props.fullProfile}
             />
          </View>

          <View style={styles.boxUserInfo}>
            <UserInfoBox
              profile={this.props.fullProfile}
              tracestats={this.props.tracestats}
              navigation={this.props.screenProps.rootNav}
             />
          </View>
        </View>

        <View style={styles.container}>
          <ScrollView style={{ position: 'relative' }}>
            <Text note style={{ textAlign: 'center', color: 'rgba(0,0,0,0)' }}>
             Toque nos ícones e saiba o que fazer
            </Text>

            {this._renderMissionsIcon()}

            {this._renderLogoPovoado()}
          </ScrollView>
          <Animatable.View
            animation='pulse'
            easing='ease-out'
            iterationCount='infinite'
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              top: 130,
              right: 0,
              left: 0,
              bottom: 0
            }}
          >
            <TouchableOpacity onPress={() => {
              this.setState({ showZoomer: true }, () => this.refs.zoomer.fadeIn(700))
            }}>
              <Icon
                name='search-plus'
                size={40}
                style={{
                  color: 'rgb(0,0,0)',
                  opacity: 0.5
                }}
              />
            </TouchableOpacity>
          </Animatable.View>
          {this.state.showZoomer ? (
            <Animatable.View
              ref='zoomer'
              style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                position: 'absolute',
                top: -6,
                right: 0,
                left: 0,
                bottom: 0
              }}
            >
            <ImageBackground
              source={ require('../../Assets/Images/opacity_map_zoom.png') }
              style={[ styles.backgroundImage, { position: 'absolute', zIndex: 0, height: this.state.size.height, width: this.state.size.width }]}   
              />
              <TouchableOpacity
                onPress={() => {
                  this.refs.zoomer.fadeOut(700).then(endState => {
                    this.setState({ showZoomer: false })
                  })
                }}
                style={{
                  position: 'absolute',
                  top: 10,
                  right: 10,
                  width: 30,
                  height: 30,
                  backgroundColor: 'rgba(0,0,0,.5)',
                  zIndex: 2,
                  borderRadius: 3,
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Icon
                  name='times'
                  size={20}
                  style={{
                    color: 'rgb(255,255,255)',
                    opacity: 0.5
                  }}
                />
              </TouchableOpacity>
              <PhotoView
                source={getMapFull(this.state.missionsActual)}
                minimumZoomScale={1}
                maximumZoomScale={5}
                scale={this.state.showZoomer ? 1 : null}
                androidScaleType='fitCenter'
                style={{ alignSelf: 'stretch', flex: 1, margin: 0 }}
              />
            </Animatable.View>
          ) : null}
        </View>
      </View>
    )
  }
}

export default connect(mapStateToProps, { getTracestatsAction, updateUserProfile })(Start)
