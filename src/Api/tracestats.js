import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})

const endpointTracestats = `${apiurl}/profile/tracestats`

const getTracestats = (token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointTracestats,
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  getTracestats
}
