import axios from 'axios'
import {
  GraphRequestManager,
  GraphRequest
} from 'react-native-fbsdk'
import { apiurl } from './api.variables'

const endpointSocialLogin = `${apiurl}/sociallogin`
const endpointSocialSignUp = `${apiurl}/socialsignup`
const endpointSignUp = `${apiurl}/signup`
const endpointLogin = `${apiurl}/login`

const socialLogin = (data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: endpointSocialLogin,
      data
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const socialSignUp = (data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: endpointSocialSignUp,
      data
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const signUp = (data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: endpointSignUp,
      data
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const logIn = (data) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: endpointLogin,
      data
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const fbGetUserData = (accessToken) => {
  return new Promise((resolve, reject) => {
    // token de acesso
    const responseInfoCallback = (error, result) => {
      if (error) {
        reject(error)
      } else {
        resolve(result)
      }
    }
    const infoRequest = new GraphRequest(
      '/me',
      {
        accessToken: accessToken,
        parameters: {
          fields: {
            string: 'email, last_name, first_name, gender, picture.width(500).height(500), hometown'
          }
        }
      },
      responseInfoCallback
    )
    // Start the graph request.
    new GraphRequestManager().addRequest(infoRequest).start()
  })
}

export {
  socialLogin,
  socialSignUp,
  signUp,
  logIn,
  fbGetUserData
}
