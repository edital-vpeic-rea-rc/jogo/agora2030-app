import { AsyncStorage } from 'react-native'
import { NavigationActions } from 'react-navigation'

const redirectIf401 = (status) => {
  if (status === 401) {
    AsyncStorage.setItem('apiToken', '', () => {
    })
    NavigationActions.navigate({ routeName: 'Initial', params: { clearProfile: true } })
  }
}

export {
  redirectIf401
}
