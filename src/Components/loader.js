import React, { Component } from 'react'
import { StyleSheet, View, ActivityIndicator } from 'react-native'
import Modal from 'react-native-modal'

export default class Loader extends Component {
  render () {
    return (
      <Modal
        avoidKeyboard={false}
        animationIn='fadeIn'
        isVisible={this.props.visible}
        style={styles.modal}
        >
        <View style={styles.inner}>
          <ActivityIndicator size={'large'} color='#000' />
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center'
  },
  inner: {
    alignSelf: 'center',
    backgroundColor: 'rgba(255,255,255,.9)',
    borderRadius: 15,
    padding: 5,
    width: 100
  }
})
