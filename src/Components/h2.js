import React, { Component } from 'react'
import { StyleSheet, Text as H } from 'react-native'

export default class H2 extends Component {
  render(){
    return (
      <H style={[ styles.f, this.props.style ]}>{ this.props.children }</H>
    )
  }
}

const styles = StyleSheet.create({
  f: {
    color: '#636363',
    fontFamily: 'Titillium Web', 
    fontWeight: 'bold',
    fontSize: 17 * 1.6,
    paddingTop: 4
  }
})