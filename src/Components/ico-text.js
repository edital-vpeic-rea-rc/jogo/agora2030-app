import React, { Component } from 'react'
import { StyleSheet, View, Dimensions } from 'react-native'
import { Thumbnail } from 'native-base'
import Text from './text'
const { width } = Dimensions.get('window')

export default class IcoText extends Component {
  render () {
    return (
      <View>
        <Thumbnail
          circle 
          source={ this.props.source }
          style={ styles.ico }
          />
        <Text {...this.props} style={[ this.props.style, styles.text ]}>{ this.props.text }</Text>
      </View>
    )
  }
} 

let _size = 55
let _fontSize = 12
if(width < 420) {
  _size = 45
  _fontSize: 10  
}

const styles = StyleSheet.create({
  ico: {
    alignSelf: 'center',    
    height: _size,
    width: _size,
    marginBottom: 2
  },
  text: {
    backgroundColor: 'transparent',
    fontSize: _fontSize,
    textAlign: 'center'
  }
})