import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class SmallBadge extends Component {
  render () {
    return (
      <View>
        <Icon name={this.props.ico} size={24} />
        <View style={styles.badge}>
          <Text style={styles.text}>{ this.props.text }</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  badge: {
    backgroundColor: 'red',
    borderRadius: 10,
    paddingVertical: 4,
    paddingHorizontal: 7,
    position: 'absolute',
    right: 6,
    top: -9
  },
  text: {
    color: '#fff',
    fontSize: 9
  }
})

// badge: {
//   marginTop: 4,
//   marginRight: 32,
//   backgroundColor: '#f44336',
//   height: 24,
//   width: 24,
//   borderRadius: 12,
//   alignItems: 'center',
//   justifyContent: 'center',
//   elevation: 4,
// },
// count: {
//   color: '#fff',
//   fontSize: 12,
//   fontWeight: 'bold',
//   marginTop: -2,
// },