import React, { Component } from 'react'
import { TouchableOpacity, Image, StyleSheet, Platform, View, DeviceEventEmitter } from 'react-native'
import H2 from './h2'
import BadgeUserNotifications from './badge-user-notifications'

export default class AppHeader extends Component {
  _goBack () {
    this.props.navigation.goBack()
    if (this.props.eventId) {
      DeviceEventEmitter.emit(this.props.eventId, {})
    }
  }
  renderLeft () {
    let leftButtons = []
    if (this.props.backButton) {
      leftButtons.push(
        <TouchableOpacity onPress={() => this._goBack()} key={Math.random()}>
          <Image
            source={require('../Assets/Images/ico_back.png')}
            style={styles.icoBack}
            />
        </TouchableOpacity>
      )
    }

    return leftButtons
  }

  renderRight () {
    let rightButtons = []
    if (this.props.config) {
      rightButtons.push(
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Config')} key={Math.random()}>
          <Image
            source={require('../Assets/Images/ico_cog.png')}
            style={styles.icoConfig}
            />
        </TouchableOpacity>
      )
    }
    if (this.props.notifyIcon) {
      rightButtons.push(<BadgeUserNotifications key={Math.random()}>99</BadgeUserNotifications>)
    }

    return rightButtons
  }

  render () {
    return (
      <View {...this.props} style={styles.mainHeader}>
        <View style={[ styles.row, styles.headerBar ]}>
          {/* <Statusbar backgroundColor='#fff' barStyle='dark-content'  /> */}
          <View
            style={styles.backButton}
          >
            {this.renderLeft()}
          </View>

          <View>
            <H2 style={{ color: '#4ba146', lineHeight: 30 }}>{this.props.title}</H2>
          </View>

          <View style={{minWidth: 45}}>
            { this.renderRight() }
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    ...Platform.select({
      android: {
        backgroundColor: '#fff',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
      }
    })
  },
  mainHeader: {

    borderColor: 'transparent',
    borderWidth: 0,
    height: 80,
    ...Platform.select({
      ios: {
        backgroundColor: '#fff',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
      }
    })

  },
  headerBar: {

    justifyContent: 'space-between',
    ...Platform.select({
      ios: {
        marginHorizontal: 20,
        marginTop: 40
      },
      android: {
        paddingTop: 20,
        paddingHorizontal: 10,
        paddingRight: 18,
        flex: 1
      }
    })
  },
  backButton: {
    ...Platform.select({
      android: {
        marginTop: 10
      }
    }),
    minWidth: 45
  },
  icoBack: {
    marginTop: -7,
    height: 45,
    width: 45
  },
  icoConfig: {
    height: 40,
    width: 40,
    marginTop: -10,
    marginRight: 5,
    ...Platform.select({
      android: {
        marginTop: -2,
        marginRight: -2
      }
    })
  },
  header: {
    backgroundColor: '#dae4ea',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    // height: 160,
    marginBottom: 20,
    paddingTop: 40

  }
})
