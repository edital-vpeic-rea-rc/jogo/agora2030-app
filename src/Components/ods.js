import React, { Component } from 'react'
import { Image, StyleSheet } from 'react-native'


export default class ODS extends Component {
  render() {
    return (
      <Image 
        source={ require('../Assets/Images/line_colored.png') }
        style={ styles.line }
        />
    )
  }
}

const styles = StyleSheet.create({
  line: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 5,
    width: null
  }
})