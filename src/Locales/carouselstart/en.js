export default {
  one: {
    title: 'Olá',
    intro: 'No 1º Desafio Ágora 2030 você irá realizar a curadoria de 40 soluções.',
    text: 'O cenário é o povoado Ágora.',
    text1: 'Nossos antepassados contam que em outros tempos havia muitos rios de águas transparentes, plantações que alimentavam toda a gente, crianças brincando nas praças em segurança e um castelo onde os cidadãos se reuniam para formar parcerias e criar políticas inclusivas, garantindo a dignidade e igualdade de direitos de todos os moradores...'
  },
  one1: {
    text: 'Agora é diferente. No lugar onde vivemos há pouca vegetação, a água é escassa e contaminada e a poluição torna o ar quase irrespirável.',
    text1: 'Mesmo assim, há esperança. São ideias e iniciativas da população, dos cientistas e pesquisadores, de instituições, do governo e da sociedade. O que precisamos é colocá-las em prática para transformar a nossa realidade e das próximas gerações.',
    text2: 'Só assim poderemos voltar a ver os rios correrem limpos, acabar com a fome e as doenças e garantir uma vida saudável em harmonia com a natureza.',
    text3: 'Mas, não conseguimos isso sozinhos! Participando do jogo, você muda o povoado Ágora e ajuda a mudar o mundo. Vamos começar?'
  },
  two: {
    title: 'Suas missões',
    text: 'Em cada missão você precisa cumprir um conjunto de tarefas de curadoria das soluções para passar para a seguinte.',
    text1: 'Cada missão corresponde a um dos cinco P’s dos Objetivos de Desenvolvimento Sustentável para a Agenda 2030. Ao completar estas, você é convidado a participar de uma missão especial de certificação de curadoria.'
  },
  three: {
    title: 'Como funciona',
    text: 'Esse é um desafio onde suas tarefas são relacionadas à curadoria das soluções. Você irá realizar as seguintes ações abaixo:',
    text1: 'Toque nos icones para saber mais',
    view: 'Ler as soluções participantes do desafio.',
    like: 'Curtir as soluções que achar mais relevantes para você e para um mundo sustentável.',
    favorite: 'Guarde como favorita as iniciativas que mais te interessarem.',
    share: 'Existem soluções que deveriam estar circulando para todo mundo saber que elas existem.',
    share1: 'Você precisa compartilhar essas nas suas redes sociais.',
    comment: 'Ficou com alguma dúvida ou quer contribuir com alguma ideia para melhorar uma solução? Escreva no espaço de comentários.',
    donate: 'Para fazer uma solução crescer mais, você pode investir seeds nelas. Seed é a moeda oficial do povoado Ágora.',
    donate1: 'Ao iniciar o jogo você recebe 100 “seeds”. Ao longo do desafio você acumula mais pontos e ao completar a terceira fase, você terá a quantidade de pontos necessários para desbloquear os seeds que você acumulou até ali.'
  },
  four: {
    title: 'Seja um curador',
    text: 'A cada missão cumprida, você transforma povoado e, ao final, recebe um certificado de curador.',
    text1: 'Vamos começar?',
    button: 'Sim! Vamos lá.'
  }
}
