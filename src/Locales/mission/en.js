export default {
  pessoas_completed: {
    title: 'Você ganhou o selo Pessoas!',
    intro: 'Como reconhecimento e gratidão do povo de Ágora, que graças a sua ajuda evoluiu, como se pode ver no mapa.',
    text: 'A Agenda 2030 está determinada a acabar com a pobreza e a fome e garantir que todos os seres humanos possam realizar o seu potencial em dignidade e igualdade, em um ambiente saudável.',
    text1: 'Continue a ajudar o Povoado Ágora até se tornar um Curador do Desafio Ágora 2030!'
  },
  planeta_completed: {
    title: 'Você ganhou o selo Planeta!',
    intro: 'Como reconhecimento e gratidão do povo de Ágora, que graças a sua ajuda evoluiu, como se pode ver no mapa.',
    text: 'A Agenda 2030 está determinada a proteger o planeta da degradação, sobretudo por meio do consumo e da produção sustentáveis, da gestão sustentável dos seus recursos naturais e tomando medidas urgentes sobre a mudança climática.',
    text1: 'Continue a ajudar o Povoado Ágora até se tornar um Curador do Desafio Ágora 2030!'
  },
  prosperidade_completed: {
    title: 'Você ganhou o selo Prosperidade!',
    intro: 'Como reconhecimento e gratidão do povo de Ágora, que graças a sua ajuda evoluiu, como se pode ver no mapa.',
    text: 'A Agenda 2030 está determinada a assegurar que todos os seres humanos possam desfrutar de uma vida próspera e de plena realização pessoal, e que o progresso econômico, social e tecnológico ocorra em harmonia com a natureza.',
    text1: 'Continue a ajudar o Povoado Ágora até se tornar um Curador do Desafio Ágora 2030!'
  },
  parceria_completed: {
    title: 'Você ganhou o selo Parceria!',
    intro: 'Como reconhecimento e gratidão do povo de Ágora, que graças a sua ajuda evoluiu, como se pode ver no mapa.',
    text: 'A Agenda 2030 está determinada a mobilizar os meios necessários para ser implementada por meio de uma Parceria Global, com base num espírito de solidariedade, com a participação de todos os países, todas as partes interessadas e todas as pessoas.',
    text1: 'Continue a ajudar o Povoado Ágora até se tornar um Curador do Desafio Ágora 2030!'
  },
  paz_completed: {
    title: 'Você ganhou o selo Paz!',
    intro: 'Como reconhecimento e gratidão do povo de Ágora, que graças a sua ajuda evoluiu, como se pode ver no mapa.',
    text: 'A Agenda 2030 está determinada a promover sociedades pacíficas, justas e inclusivas que estão livres do medo e da violência. Não pode haver desenvolvimento sustentável sem paz e não há paz sem desenvolvimento sustentável.',
    text1: 'Continue a ajudar o Povoado Ágora até se tornar um Curador do Desafio Ágora 2030!'
  },
  curadoria_completed: {
    title: 'Você ganhou o selo de Curador do Desafio 2030!',
    intro: 'Como reconhecimento e gratidão do povo de Ágora temos uma homenagem pra você no nosso povoado!',
    text: 'Mas a sua missão não acabou. Continue a contribuir com as Estratégias dos Objetivos do Desenvolvimento Sustentável da Agenda 2030.',
    text1: 'Acompanhe e compartilhe as próximas edições e as outras iniciativas no site agora2030.fiocruz.br, continuando esse corrente de mobilização.'
  }
}
