import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { AsyncStorage, DeviceEventEmitter } from 'react-native'
import { persistStore, getStoredState, createPersistor } from 'redux-persist'
import { Root } from 'native-base'
import GoogleSignIn from 'react-native-google-sign-in'
import OneSignal from 'react-native-onesignal'
import RootNavigator from './Scenes/Routes'
import appStore from './Redux'

export default class App extends Component {
  initPersist (repopulate = false) {
    persistStore(appStore, {storage: AsyncStorage, blacklist: ['nav', 'Auth']}, () => {
      console.log('iniciada persistencia do Redux Store')
      if (repopulate) {
        let persistor = createPersistor(appStore, { storage: AsyncStorage })
        getStoredState({storage: AsyncStorage, blacklist: ['nav', 'Auth']}, (err, state) => {
          if (err) {
            console.log(err)
          }
          console.log('repopulando o state do app', state)
          persistor.rehydrate(state.UserProfile)
          persistor.rehydrate(state.Tracestats)
        })
      }
    })
  }

  componentDidMount () {
    GoogleSignIn.configure({
      // iOS
      clientID: '115773860068-h721qc3n728bn1uq7f9pt3km31khpcgv.apps.googleusercontent.com',
      // debug id
      // clientID: '155548439815-tmb1q8fcipdr0l6rtor52pl7heutj0pk.apps.googleusercontent.com',

      // iOS, Android
      // https://developers.google.com/identity/protocols/googlescopes
      scopes: ['profile'],

      // iOS, Android
      // Whether to request email and basic profile.
      // [Default: true]
      // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a06bf16b507496b126d25ea909d366ba4
      shouldFetchBasicProfile: true,

      // iOS
      // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a486c8df263ca799bea18ebe5430dbdf7
      // language: string,

      // iOS
      // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd
      // loginHint: string,

      // iOS, Android
      // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#ae214ed831bb93a06d8d9c3692d5b35f9
      // serverClientID: 'yourServerClientID',

      // Android
      // Whether to request server auth code. Make sure to provide `serverClientID`.
      // https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInOptions.Builder.html#requestServerAuthCode(java.lang.String, boolean)
      offlineAccess: true,

      // Android
      // Whether to force code for refresh token.
      // https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInOptions.Builder.html#requestServerAuthCode(java.lang.String, boolean)
      forceCodeForRefreshToken: true,

      // iOS
      // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a211c074872cd542eda53f696c5eef871
      // openIDRealm: string,

      // Android
      // https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInOptions.Builder.html#setAccountName(java.lang.String)
      accountName: '',

      // iOS, Android
      // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a6d85d14588e8bf21a4fcf63e869e3be3
      hostedDomain: ''
    })
  }

  componentWillMount () {
    // persistStore(appStore, {storage: AsyncStorage, blacklist: ['nav', 'Auth']}, () => {
    //   console.log('restored')
    // })
    // .purge()
    // AsyncStorage.removeItem('apiToken', () => false)
    DeviceEventEmitter.addListener('initPersistence', (data) => {
      // console.log(data)
      this.initPersist(data.repopulate)
    })

    OneSignal.addEventListener('received', this.onReceived)
    OneSignal.addEventListener('opened', this.onOpened)
    OneSignal.addEventListener('registered', this.onRegistered)
    OneSignal.addEventListener('ids', this.onIds)
  }

  componentWillUnmount () {
    OneSignal.removeEventListener('received', this.onReceived)
    OneSignal.removeEventListener('opened', this.onOpened)
    OneSignal.removeEventListener('registered', this.onRegistered)
    OneSignal.removeEventListener('ids', this.onIds)
    if (this._restoreNavigationState) {
      this._restoreNavigationState()
    }
  }

  onReceived (notification) {
    // console.log("Notification received: ", notification);
  }

  onOpened (openResult) {
    // console.log('Message: ', openResult.notification.payload.body);
    // console.log('Data: ', openResult.notification.payload.additionalData);
    // console.log('isActive: ', openResult.notification.isAppInFocus);
    // console.log('openResult: ', openResult);
  }

  onRegistered (notifData) {
    // console.log("Device had been registered for push notifications!", notifData);
  }

  onIds (device) {
    AsyncStorage.getItem('reduxPersist:UserProfile', (err, profile) => {
      if (!err && profile) {
        const userData = JSON.parse(profile)
        OneSignal.sendTag('id', userData.id)
      }
    })
  }

  render () {
    return (
      <Provider store={appStore} >
        <Root>
          <RootNavigator />
        </Root>
      </Provider>
    )
  }
}
