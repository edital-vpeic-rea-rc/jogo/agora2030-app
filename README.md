# Aplicativo ágora 2030

> Projeto escrito em React Native para as plataformas Android e Ios.

## Requisitos e instalação

 - nodejs: ">= 4.0.0"
 - npm: ">= 3.0.0"
 - react-native": "0.48.3

``` bash

# Dentro da raiz do projeto instale as dependências:
yarn install

# Para executar em ambiente de desenvolvimento (é necessário possuir as sdks de cada ambiente)
`npm run android` or `npm run ios` (apenas mac)

# Na pasta ```/src/Api``` existe um arquivo api.variables onde devem ser informadas as variaveis do ambiente
```


Sobre o [react-navite](https://facebook.github.io/react-native/docs/getting-started.html)